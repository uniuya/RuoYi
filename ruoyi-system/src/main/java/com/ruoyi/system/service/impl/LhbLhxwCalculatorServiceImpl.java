package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LhbLhxwCalculatorMapper;
import com.ruoyi.system.domain.LhbLhxwCalculator;
import com.ruoyi.system.service.ILhbLhxwCalculatorService;
import com.ruoyi.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
@Service
public class LhbLhxwCalculatorServiceImpl implements ILhbLhxwCalculatorService 
{
    @Autowired
    private LhbLhxwCalculatorMapper lhbLhxwCalculatorMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public LhbLhxwCalculator selectLhbLhxwCalculatorByTradeDate(String tradeDate)
    {
        return lhbLhxwCalculatorMapper.selectLhbLhxwCalculatorByTradeDate(tradeDate);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param lhbLhxwCalculator 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<LhbLhxwCalculator> selectLhbLhxwCalculatorList(LhbLhxwCalculator lhbLhxwCalculator)
    {
        return lhbLhxwCalculatorMapper.selectLhbLhxwCalculatorList(lhbLhxwCalculator);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param lhbLhxwCalculator 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertLhbLhxwCalculator(LhbLhxwCalculator lhbLhxwCalculator)
    {
        return lhbLhxwCalculatorMapper.insertLhbLhxwCalculator(lhbLhxwCalculator);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param lhbLhxwCalculator 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateLhbLhxwCalculator(LhbLhxwCalculator lhbLhxwCalculator)
    {
        return lhbLhxwCalculatorMapper.updateLhbLhxwCalculator(lhbLhxwCalculator);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteLhbLhxwCalculatorByTradeDates(String tradeDates)
    {
        return lhbLhxwCalculatorMapper.deleteLhbLhxwCalculatorByTradeDates(Convert.toStrArray(tradeDates));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteLhbLhxwCalculatorByTradeDate(String tradeDate)
    {
        return lhbLhxwCalculatorMapper.deleteLhbLhxwCalculatorByTradeDate(tradeDate);
    }
}
