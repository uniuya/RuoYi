package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LhbCalculatorMapper;
import com.ruoyi.system.domain.LhbCalculator;
import com.ruoyi.system.service.ILhbCalculatorService;
import com.ruoyi.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
@Service
public class LhbCalculatorServiceImpl implements ILhbCalculatorService 
{
    @Autowired
    private LhbCalculatorMapper lhbCalculatorMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public LhbCalculator selectLhbCalculatorByTradeDate(String tradeDate)
    {
        return lhbCalculatorMapper.selectLhbCalculatorByTradeDate(tradeDate);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<LhbCalculator> selectLhbCalculatorList(LhbCalculator lhbCalculator)
    {
        return lhbCalculatorMapper.selectLhbCalculatorList(lhbCalculator);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertLhbCalculator(LhbCalculator lhbCalculator)
    {
        return lhbCalculatorMapper.insertLhbCalculator(lhbCalculator);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateLhbCalculator(LhbCalculator lhbCalculator)
    {
        return lhbCalculatorMapper.updateLhbCalculator(lhbCalculator);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteLhbCalculatorByTradeDates(String tradeDates)
    {
        return lhbCalculatorMapper.deleteLhbCalculatorByTradeDates(Convert.toStrArray(tradeDates));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteLhbCalculatorByTradeDate(String tradeDate)
    {
        return lhbCalculatorMapper.deleteLhbCalculatorByTradeDate(tradeDate);
    }
}
