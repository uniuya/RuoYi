package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DailyHotBkStocksMapper;
import com.ruoyi.system.domain.DailyHotBkStocks;
import com.ruoyi.system.service.IDailyHotBkStocksService;
import com.ruoyi.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-08-01
 */
@Service
public class DailyHotBkStocksServiceImpl implements IDailyHotBkStocksService 
{
    @Autowired
    private DailyHotBkStocksMapper dailyHotBkStocksMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public List<DailyHotBkStocks> selectDailyHotBkStocksByTradeDate(String tradeDate)
    {
        return dailyHotBkStocksMapper.selectDailyHotBkStocksByTradeDate(tradeDate);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dailyHotBkStocks 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<DailyHotBkStocks> selectDailyHotBkStocksList(DailyHotBkStocks dailyHotBkStocks)
    {
        return dailyHotBkStocksMapper.selectDailyHotBkStocksList(dailyHotBkStocks);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param dailyHotBkStocks 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDailyHotBkStocks(DailyHotBkStocks dailyHotBkStocks)
    {
        return dailyHotBkStocksMapper.insertDailyHotBkStocks(dailyHotBkStocks);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param dailyHotBkStocks 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDailyHotBkStocks(DailyHotBkStocks dailyHotBkStocks)
    {
        return dailyHotBkStocksMapper.updateDailyHotBkStocks(dailyHotBkStocks);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDailyHotBkStocksByTradeDates(String tradeDates)
    {
        return dailyHotBkStocksMapper.deleteDailyHotBkStocksByTradeDates(Convert.toStrArray(tradeDates));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteDailyHotBkStocksByTradeDate(String tradeDate)
    {
        return dailyHotBkStocksMapper.deleteDailyHotBkStocksByTradeDate(tradeDate);
    }
}
