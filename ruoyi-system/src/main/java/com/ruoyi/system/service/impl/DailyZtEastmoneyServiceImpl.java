package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DailyZtEastmoneyMapper;
import com.ruoyi.system.domain.DailyZtEastmoney;
import com.ruoyi.system.service.IDailyZtEastmoneyService;
import com.ruoyi.common.core.text.Convert;

/**
 * 涨停Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
@Service
public class DailyZtEastmoneyServiceImpl implements IDailyZtEastmoneyService 
{
    @Autowired
    private DailyZtEastmoneyMapper dailyZtEastmoneyMapper;

    /**
     * 查询涨停
     * 
     * @param id 涨停主键
     * @return 涨停
     */
    @Override
    public DailyZtEastmoney selectDailyZtEastmoneyById(Long id)
    {
        return dailyZtEastmoneyMapper.selectDailyZtEastmoneyById(id);
    }

    /**
     * 查询涨停列表
     * 
     * @param dailyZtEastmoney 涨停
     * @return 涨停
     */
    @Override
    public List<DailyZtEastmoney> selectDailyZtEastmoneyList(DailyZtEastmoney dailyZtEastmoney)
    {
        return dailyZtEastmoneyMapper.selectDailyZtEastmoneyList(dailyZtEastmoney);
    }

    /**
     * 新增涨停
     * 
     * @param dailyZtEastmoney 涨停
     * @return 结果
     */
    @Override
    public int insertDailyZtEastmoney(DailyZtEastmoney dailyZtEastmoney)
    {
        return dailyZtEastmoneyMapper.insertDailyZtEastmoney(dailyZtEastmoney);
    }

    /**
     * 修改涨停
     * 
     * @param dailyZtEastmoney 涨停
     * @return 结果
     */
    @Override
    public int updateDailyZtEastmoney(DailyZtEastmoney dailyZtEastmoney)
    {
        dailyZtEastmoney.setUpdateTime(DateUtils.getNowDate());
        return dailyZtEastmoneyMapper.updateDailyZtEastmoney(dailyZtEastmoney);
    }

    /**
     * 批量删除涨停
     * 
     * @param ids 需要删除的涨停主键
     * @return 结果
     */
    @Override
    public int deleteDailyZtEastmoneyByIds(String ids)
    {
        return dailyZtEastmoneyMapper.deleteDailyZtEastmoneyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除涨停信息
     * 
     * @param id 涨停主键
     * @return 结果
     */
    @Override
    public int deleteDailyZtEastmoneyById(Long id)
    {
        return dailyZtEastmoneyMapper.deleteDailyZtEastmoneyById(id);
    }

    @Override
    public List<DailyZtEastmoney> selectLbByTradeDate(String tradeDate) {
        return dailyZtEastmoneyMapper.selectLbByTradeDate(tradeDate);
    }
}
