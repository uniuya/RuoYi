package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LhbCalculator;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
public interface ILhbCalculatorService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public LhbCalculator selectLhbCalculatorByTradeDate(String tradeDate);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<LhbCalculator> selectLhbCalculatorList(LhbCalculator lhbCalculator);

    /**
     * 新增【请填写功能名称】
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 结果
     */
    public int insertLhbCalculator(LhbCalculator lhbCalculator);

    /**
     * 修改【请填写功能名称】
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 结果
     */
    public int updateLhbCalculator(LhbCalculator lhbCalculator);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteLhbCalculatorByTradeDates(String tradeDates);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteLhbCalculatorByTradeDate(String tradeDate);
}
