package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.LhbCalculator;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
public interface LhbCalculatorMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public LhbCalculator selectLhbCalculatorByTradeDate(String tradeDate);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<LhbCalculator> selectLhbCalculatorList(LhbCalculator lhbCalculator);

    /**
     * 新增【请填写功能名称】
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 结果
     */
    public int insertLhbCalculator(LhbCalculator lhbCalculator);

    /**
     * 修改【请填写功能名称】
     * 
     * @param lhbCalculator 【请填写功能名称】
     * @return 结果
     */
    public int updateLhbCalculator(LhbCalculator lhbCalculator);

    /**
     * 删除【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteLhbCalculatorByTradeDate(String tradeDate);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLhbCalculatorByTradeDates(String[] tradeDates);
}
