package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.LhbLhxwCalculator;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
public interface LhbLhxwCalculatorMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public LhbLhxwCalculator selectLhbLhxwCalculatorByTradeDate(String tradeDate);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param lhbLhxwCalculator 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<LhbLhxwCalculator> selectLhbLhxwCalculatorList(LhbLhxwCalculator lhbLhxwCalculator);

    /**
     * 新增【请填写功能名称】
     * 
     * @param lhbLhxwCalculator 【请填写功能名称】
     * @return 结果
     */
    public int insertLhbLhxwCalculator(LhbLhxwCalculator lhbLhxwCalculator);

    /**
     * 修改【请填写功能名称】
     * 
     * @param lhbLhxwCalculator 【请填写功能名称】
     * @return 结果
     */
    public int updateLhbLhxwCalculator(LhbLhxwCalculator lhbLhxwCalculator);

    /**
     * 删除【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteLhbLhxwCalculatorByTradeDate(String tradeDate);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLhbLhxwCalculatorByTradeDates(String[] tradeDates);
}
