package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DailyZtEastmoney;

/**
 * 涨停Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
public interface DailyZtEastmoneyMapper 
{
    /**
     * 查询涨停
     * 
     * @param id 涨停主键
     * @return 涨停
     */
    public DailyZtEastmoney selectDailyZtEastmoneyById(Long id);

    /**
     * 查询涨停列表
     * 
     * @param dailyZtEastmoney 涨停
     * @return 涨停集合
     */
    public List<DailyZtEastmoney> selectDailyZtEastmoneyList(DailyZtEastmoney dailyZtEastmoney);

    /**
     * 新增涨停
     * 
     * @param dailyZtEastmoney 涨停
     * @return 结果
     */
    public int insertDailyZtEastmoney(DailyZtEastmoney dailyZtEastmoney);

    /**
     * 修改涨停
     * 
     * @param dailyZtEastmoney 涨停
     * @return 结果
     */
    public int updateDailyZtEastmoney(DailyZtEastmoney dailyZtEastmoney);

    /**
     * 删除涨停
     * 
     * @param id 涨停主键
     * @return 结果
     */
    public int deleteDailyZtEastmoneyById(Long id);

    /**
     * 批量删除涨停
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDailyZtEastmoneyByIds(String[] ids);



    List<DailyZtEastmoney> selectLbByTradeDate(String tradeDate);
}
