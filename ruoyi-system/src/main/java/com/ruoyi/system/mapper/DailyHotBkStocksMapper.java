package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DailyHotBkStocks;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-08-01
 */
public interface DailyHotBkStocksMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public List<DailyHotBkStocks> selectDailyHotBkStocksByTradeDate(String tradeDate);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param dailyHotBkStocks 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<DailyHotBkStocks> selectDailyHotBkStocksList(DailyHotBkStocks dailyHotBkStocks);

    /**
     * 新增【请填写功能名称】
     * 
     * @param dailyHotBkStocks 【请填写功能名称】
     * @return 结果
     */
    public int insertDailyHotBkStocks(DailyHotBkStocks dailyHotBkStocks);

    /**
     * 修改【请填写功能名称】
     * 
     * @param dailyHotBkStocks 【请填写功能名称】
     * @return 结果
     */
    public int updateDailyHotBkStocks(DailyHotBkStocks dailyHotBkStocks);

    /**
     * 删除【请填写功能名称】
     * 
     * @param tradeDate 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteDailyHotBkStocksByTradeDate(String tradeDate);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param tradeDates 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDailyHotBkStocksByTradeDates(String[] tradeDates);
}
