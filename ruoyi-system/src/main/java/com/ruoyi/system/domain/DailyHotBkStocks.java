package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 daily_hot_bk_stocks
 * 
 * @author ruoyi
 * @date 2024-08-01
 */
public class DailyHotBkStocks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tradeDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String code;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String prodName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long curPrice;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long pxChangeRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long circulationValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String description;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long enterTime;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer upLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String plates;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long timeOnMarket;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long turnoverRatio;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String mDaysNBoards;

    public void setTradeDate(String tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public String getTradeDate() 
    {
        return tradeDate;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setProdName(String prodName) 
    {
        this.prodName = prodName;
    }

    public String getProdName() 
    {
        return prodName;
    }
    public void setCurPrice(Long curPrice) 
    {
        this.curPrice = curPrice;
    }

    public Long getCurPrice() 
    {
        return curPrice;
    }
    public void setPxChangeRate(Long pxChangeRate) 
    {
        this.pxChangeRate = pxChangeRate;
    }

    public Long getPxChangeRate() 
    {
        return pxChangeRate;
    }
    public void setCirculationValue(Long circulationValue) 
    {
        this.circulationValue = circulationValue;
    }

    public Long getCirculationValue() 
    {
        return circulationValue;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setEnterTime(Long enterTime) 
    {
        this.enterTime = enterTime;
    }

    public Long getEnterTime() 
    {
        return enterTime;
    }
    public void setUpLimit(Integer upLimit) 
    {
        this.upLimit = upLimit;
    }

    public Integer getUpLimit() 
    {
        return upLimit;
    }
    public void setPlates(String plates) 
    {
        this.plates = plates;
    }

    public String getPlates() 
    {
        return plates;
    }
    public void setTimeOnMarket(Long timeOnMarket) 
    {
        this.timeOnMarket = timeOnMarket;
    }

    public Long getTimeOnMarket() 
    {
        return timeOnMarket;
    }
    public void setTurnoverRatio(Long turnoverRatio) 
    {
        this.turnoverRatio = turnoverRatio;
    }

    public Long getTurnoverRatio() 
    {
        return turnoverRatio;
    }
    public void setmDaysNBoards(String mDaysNBoards) 
    {
        this.mDaysNBoards = mDaysNBoards;
    }

    public String getmDaysNBoards() 
    {
        return mDaysNBoards;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tradeDate", getTradeDate())
            .append("code", getCode())
            .append("prodName", getProdName())
            .append("curPrice", getCurPrice())
            .append("pxChangeRate", getPxChangeRate())
            .append("circulationValue", getCirculationValue())
            .append("description", getDescription())
            .append("enterTime", getEnterTime())
            .append("upLimit", getUpLimit())
            .append("plates", getPlates())
            .append("timeOnMarket", getTimeOnMarket())
            .append("turnoverRatio", getTurnoverRatio())
            .append("mDaysNBoards", getmDaysNBoards())
            .toString();
    }
}
