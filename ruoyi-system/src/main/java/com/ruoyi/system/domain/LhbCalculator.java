package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 lhb_calculator
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
public class LhbCalculator extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tradeDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String stockCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String stockName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String buyCount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sellCount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long jBuyValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long jSellValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long jBuyNet;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long gBuyValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long gSellValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long gBuyNet;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long type;

    /** 营业部类别 默认为1--机构 */
    @Excel(name = "营业部类别 默认为1--机构")
    private Long yyb;

    public void setTradeDate(String tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public String getTradeDate() 
    {
        return tradeDate;
    }
    public void setStockCode(String stockCode) 
    {
        this.stockCode = stockCode;
    }

    public String getStockCode() 
    {
        return stockCode;
    }
    public void setStockName(String stockName) 
    {
        this.stockName = stockName;
    }

    public String getStockName() 
    {
        return stockName;
    }
    public void setBuyCount(String buyCount) 
    {
        this.buyCount = buyCount;
    }

    public String getBuyCount() 
    {
        return buyCount;
    }
    public void setSellCount(String sellCount) 
    {
        this.sellCount = sellCount;
    }

    public String getSellCount() 
    {
        return sellCount;
    }
    public void setjBuyValue(Long jBuyValue) 
    {
        this.jBuyValue = jBuyValue;
    }

    public Long getjBuyValue() 
    {
        return jBuyValue;
    }
    public void setjSellValue(Long jSellValue) 
    {
        this.jSellValue = jSellValue;
    }

    public Long getjSellValue() 
    {
        return jSellValue;
    }
    public void setjBuyNet(Long jBuyNet) 
    {
        this.jBuyNet = jBuyNet;
    }

    public Long getjBuyNet() 
    {
        return jBuyNet;
    }
    public void setgBuyValue(Long gBuyValue) 
    {
        this.gBuyValue = gBuyValue;
    }

    public Long getgBuyValue() 
    {
        return gBuyValue;
    }
    public void setgSellValue(Long gSellValue) 
    {
        this.gSellValue = gSellValue;
    }

    public Long getgSellValue() 
    {
        return gSellValue;
    }
    public void setgBuyNet(Long gBuyNet) 
    {
        this.gBuyNet = gBuyNet;
    }

    public Long getgBuyNet() 
    {
        return gBuyNet;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setYyb(Long yyb) 
    {
        this.yyb = yyb;
    }

    public Long getYyb() 
    {
        return yyb;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tradeDate", getTradeDate())
            .append("stockCode", getStockCode())
            .append("stockName", getStockName())
            .append("buyCount", getBuyCount())
            .append("sellCount", getSellCount())
            .append("jBuyValue", getjBuyValue())
            .append("jSellValue", getjSellValue())
            .append("jBuyNet", getjBuyNet())
            .append("gBuyValue", getgBuyValue())
            .append("gSellValue", getgSellValue())
            .append("gBuyNet", getgBuyNet())
            .append("type", getType())
            .append("yyb", getYyb())
            .toString();
    }
}
