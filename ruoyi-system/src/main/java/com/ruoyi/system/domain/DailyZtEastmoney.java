package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 涨停对象 daily_zt_eastmoney
 * 
 * @author ruoyi
 * @date 2024-04-06
 */
public class DailyZtEastmoney extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tradeDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String symbol;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /** 收盘价 */
    @Excel(name = "收盘价")
    private String closePrice;

    /** 涨跌幅 */
    @Excel(name = "涨跌幅")
    private String pctChg;

    /** 成交额（万） */
    @Excel(name = "成交额", readConverterExp = "万=")
    private String tradeMoney;

    /** 流通市值 */
    @Excel(name = "流通市值")
    private String circulatedValue;

    /** 总市值 */
    @Excel(name = "总市值")
    private String allValue;

    /** 换手率 */
    @Excel(name = "换手率")
    private String turnoverRate;

    /** 封板资金 */
    @Excel(name = "封板资金")
    private String fdMoney;

    /** 首次涨停时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "首次涨停时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date firstTime;

    /** 最后封板时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后封板时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastTime;

    /** 打开次数 */
    @Excel(name = "打开次数")
    private String openTimes;

    /** 涨停统计 */
    @Excel(name = "涨停统计")
    private String strth;

    /** 连板次数 */
    @Excel(name = "连板次数")
    private String limitTimes;

    /** 是否是高度旗帜 */
    @Excel(name = "是否是高度旗帜")
    private Integer flag;

    /** 行业 */
    @Excel(name = "行业")
    private String industry;

    /** 新股未开板 */
    @Excel(name = "新股未开板")
    private Integer newOne;

    /** 理由 */
    @Excel(name = "理由")
    private String reason;

    /** 备注 */
    @Excel(name = "备注")
    private String comment;

    /** 涨停类型 1-一字板  2-换手板 */
    @Excel(name = "涨停类型 1-一字板  2-换手板")
    private Integer type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTradeDate(String tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public String getTradeDate() 
    {
        return tradeDate;
    }
    public void setSymbol(String symbol) 
    {
        this.symbol = symbol;
    }

    public String getSymbol() 
    {
        return symbol;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setClosePrice(String closePrice) 
    {
        this.closePrice = closePrice;
    }

    public String getClosePrice() 
    {
        return closePrice;
    }
    public void setPctChg(String pctChg) 
    {
        this.pctChg = pctChg;
    }

    public String getPctChg() 
    {
        return pctChg;
    }
    public void setTradeMoney(String tradeMoney) 
    {
        this.tradeMoney = tradeMoney;
    }

    public String getTradeMoney() 
    {
        return tradeMoney;
    }
    public void setCirculatedValue(String circulatedValue) 
    {
        this.circulatedValue = circulatedValue;
    }

    public String getCirculatedValue() 
    {
        return circulatedValue;
    }
    public void setAllValue(String allValue) 
    {
        this.allValue = allValue;
    }

    public String getAllValue() 
    {
        return allValue;
    }
    public void setTurnoverRate(String turnoverRate) 
    {
        this.turnoverRate = turnoverRate;
    }

    public String getTurnoverRate() 
    {
        return turnoverRate;
    }
    public void setFdMoney(String fdMoney) 
    {
        this.fdMoney = fdMoney;
    }

    public String getFdMoney() 
    {
        return fdMoney;
    }
    public void setFirstTime(Date firstTime) 
    {
        this.firstTime = firstTime;
    }

    public Date getFirstTime() 
    {
        return firstTime;
    }
    public void setLastTime(Date lastTime) 
    {
        this.lastTime = lastTime;
    }

    public Date getLastTime() 
    {
        return lastTime;
    }
    public void setOpenTimes(String openTimes) 
    {
        this.openTimes = openTimes;
    }

    public String getOpenTimes() 
    {
        return openTimes;
    }
    public void setStrth(String strth) 
    {
        this.strth = strth;
    }

    public String getStrth() 
    {
        return strth;
    }
    public void setLimitTimes(String limitTimes) 
    {
        this.limitTimes = limitTimes;
    }

    public String getLimitTimes() 
    {
        return limitTimes;
    }
    public void setFlag(Integer flag) 
    {
        this.flag = flag;
    }

    public Integer getFlag() 
    {
        return flag;
    }
    public void setIndustry(String industry) 
    {
        this.industry = industry;
    }

    public String getIndustry() 
    {
        return industry;
    }
    public void setNewOne(Integer newOne) 
    {
        this.newOne = newOne;
    }

    public Integer getNewOne() 
    {
        return newOne;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setComment(String comment) 
    {
        this.comment = comment;
    }

    public String getComment() 
    {
        return comment;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tradeDate", getTradeDate())
            .append("symbol", getSymbol())
            .append("name", getName())
            .append("closePrice", getClosePrice())
            .append("pctChg", getPctChg())
            .append("tradeMoney", getTradeMoney())
            .append("circulatedValue", getCirculatedValue())
            .append("allValue", getAllValue())
            .append("turnoverRate", getTurnoverRate())
            .append("fdMoney", getFdMoney())
            .append("firstTime", getFirstTime())
            .append("lastTime", getLastTime())
            .append("openTimes", getOpenTimes())
            .append("strth", getStrth())
            .append("limitTimes", getLimitTimes())
            .append("flag", getFlag())
            .append("industry", getIndustry())
            .append("newOne", getNewOne())
            .append("updateTime", getUpdateTime())
            .append("reason", getReason())
            .append("comment", getComment())
            .append("type", getType())
            .toString();
    }
}
