package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 lhb_lhxw_calculator
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
public class LhbLhxwCalculator extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tradeDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String stockCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String stockName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String buyCount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sellCount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long buyValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sellValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long buyNet;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long volValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Float percent;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long type;

    public void setTradeDate(String tradeDate) 
    {
        this.tradeDate = tradeDate;
    }

    public String getTradeDate() 
    {
        return tradeDate;
    }
    public void setStockCode(String stockCode) 
    {
        this.stockCode = stockCode;
    }

    public String getStockCode() 
    {
        return stockCode;
    }
    public void setStockName(String stockName) 
    {
        this.stockName = stockName;
    }

    public String getStockName() 
    {
        return stockName;
    }
    public void setBuyCount(String buyCount) 
    {
        this.buyCount = buyCount;
    }

    public String getBuyCount() 
    {
        return buyCount;
    }
    public void setSellCount(String sellCount) 
    {
        this.sellCount = sellCount;
    }

    public String getSellCount() 
    {
        return sellCount;
    }
    public void setBuyValue(Long buyValue) 
    {
        this.buyValue = buyValue;
    }

    public Long getBuyValue() 
    {
        return buyValue;
    }
    public void setSellValue(Long sellValue) 
    {
        this.sellValue = sellValue;
    }

    public Long getSellValue() 
    {
        return sellValue;
    }
    public void setBuyNet(Long buyNet) 
    {
        this.buyNet = buyNet;
    }

    public Long getBuyNet() 
    {
        return buyNet;
    }
    public void setVolValue(Long volValue) 
    {
        this.volValue = volValue;
    }

    public Long getVolValue() 
    {
        return volValue;
    }
    public void setPercent(Float percent)
    {
        this.percent = percent;
    }

    public Float getPercent()
    {
        return percent;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tradeDate", getTradeDate())
            .append("stockCode", getStockCode())
            .append("stockName", getStockName())
            .append("buyCount", getBuyCount())
            .append("sellCount", getSellCount())
            .append("buyValue", getBuyValue())
            .append("sellValue", getSellValue())
            .append("buyNet", getBuyNet())
            .append("volValue", getVolValue())
            .append("percent", getPercent())
            .append("type", getType())
            .toString();
    }
}
