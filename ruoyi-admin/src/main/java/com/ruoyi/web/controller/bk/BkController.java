package com.ruoyi.web.controller.bk;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.CookieUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.DailyHotBkStocks;
import com.ruoyi.system.service.IDailyHotBkStocksService;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysMenuService;
import com.ruoyi.web.controller.tool.RedisService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 首页 业务处理
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/bk")
public class BkController extends BaseController
{


    @Autowired
    private RedisService redisService;

    @Autowired
    private IDailyHotBkStocksService dailyHotBkStocksService;


    @RequiresPermissions("bk:hotConceptTree:view")
    @GetMapping("/hotConceptTree")
    public String hotConceptTree(ModelMap mmap)
    {
        String hotConceptTree = (String)redisService.get("hotConceptTree");
        mmap.put("hotConceptTree",hotConceptTree);
        return "bk/hotConceptTree";
    }

    @RequiresPermissions("bk:hotConceptTree:edit")
    @PostMapping("/hotConceptTree/edit")
    @ResponseBody
    public AjaxResult list(String hotConceptTree)
    {
        redisService.set("hotConceptTree",hotConceptTree);
        return  toAjax(1);
    }

    @RequiresPermissions("bk:recentConcept:view")
    @GetMapping("/recentConcept")
    public String recentConcept(ModelMap mmap)
    {
        String today = (String)redisService.get("today");
        Set<Object> recentConceptNames = redisService.hmgetAllKeys("recentConceptStocks");
        Map recentConceptStocks = redisService.hmget("recentConceptStocks");
        mmap.put("dailyHotBkStocksList",dailyHotBkStocksService.selectDailyHotBkStocksByTradeDate(today));
        mmap.put("recentConceptNames",recentConceptNames);
        mmap.put("recentConceptStocks",recentConceptStocks);
        return "bk/recentConcept";
    }

    @RequiresPermissions("bk:recentConcept:edit")
    @PostMapping("/recentConcept/edit")
    @ResponseBody
    public AjaxResult recentConceptEdit(String symbol,String concepts)
    {
        redisService.hset("recentConcept",symbol,concepts);
        return  toAjax(1);
    }


    @RequiresPermissions("bk:recentConcept:edit")
    @PostMapping("/recentConcept/remove")
    @ResponseBody
    public AjaxResult recentConceptRemove(String symbol)
    {
        redisService.hdel("recentConcept",symbol);
        return  toAjax(1);
    }

    @RequiresPermissions("bk:hotStockConcept:view")
    @GetMapping("/hotStockConcept")
    public String hotStockConcept(ModelMap mmap)
    {

        Map<Object,Object> dailyGoodStock = redisService.hmget("daily_good_stock");
        List<Map<String,Object>> stocks = new ArrayList<>();
        String trade_date = (String) redisService.get("today");
        dailyGoodStock.forEach((key,value)->{
            Map<String,Object> stock = new HashMap<>();
            stock.put("symbol",key);
            stock.put("name",value);
            Object recentConcept = redisService.hget("recentConcept", (String) key);
            if(recentConcept!=null){
                stock.put("concept",recentConcept);
            }
            Object conceptThs =  redisService.hget(trade_date+"_zt_reason",(String) key);
            if(conceptThs!=null){
                stock.put("conceptThs",conceptThs);
            }
            stocks.add(stock);
        });
        mmap.put("stocks",stocks);
        return "bk/hotStockConcept";
    }



    //以下为新的板块操作API
    @RequiresPermissions("bk:recentConcept:edit")
    @PostMapping("/recentConcept/edit2")
    @ResponseBody
    public AjaxResult recentConceptEdit2(String concept,String data)
    {
        if (StringUtils.isNotEmpty(concept)) {
            redisService.hset("recentConceptStocks",concept,data);
            return  toAjax(1);
        }
        return toAjax(0);

    }

    @RequiresPermissions("bk:recentConcept:edit")
    @PostMapping("/recentConcept/saveDaily")
    @ResponseBody
    public AjaxResult recentConceptSaveDaily(String data)
    {
        String today = (String)redisService.get("today");
        List<Map> dataMapList = JSON.parseArray(data, Map.class);
        for(int i=0;i<dataMapList.size();i++){
            Map<String,Object> dataMap =dataMapList.get(i);
            String dataStr = (String)dataMap.get("data");
            Map<String,Object> obj = JSON.parseObject(dataStr,Map.class);
            List<String> mainStockNames = (List)obj.get("main_stock");
//            List<String> trendNames = (List)obj.get("trend");
            List<String> lbNames = (List)obj.get("lb");
//            List<String> taoliNames = (List)obj.get("taoli");
            List<String> otherNames = (List)obj.get("other");
            for (int j=0;j<mainStockNames.size();j++) {
                data = data.replaceAll(mainStockNames.get(j),fullfillNames( mainStockNames.get(j),redisService));
            }
//            for (int j=0;j<trendNames.size();j++) {
//                data = data.replaceAll(trendNames.get(j),fullfillNames( trendNames.get(j),redisService));
//            }
            for (int j=0;j<lbNames.size();j++) {
                data = data.replaceAll(lbNames.get(j),fullfillNames( lbNames.get(j),redisService));
            }
//            for (int j=0;j<taoliNames.size();j++) {
//                data = data.replaceAll(taoliNames.get(j),fullfillNames( taoliNames.get(j),redisService));
//            }
            for (int j=0;j<otherNames.size();j++) {
                data = data.replaceAll(otherNames.get(j),fullfillNames( otherNames.get(j),redisService));
            }
        }
        redisService.hset("dailyConceptStocks",today,data);
        redisService.zSetChangeVal4SameScore("dailyConceptStocksScore",data,Long.parseLong(today.replaceAll("-","")));
        return  toAjax(1);
    }









    private String fullfillNames(String name,RedisService redisService){
        Object fullnameObj = redisService.hget("name2fullname",name);
        if(ObjectUtils.isEmpty(fullnameObj)){
            return name;
        }else{
            return (String) fullnameObj;
        }
    }

}
