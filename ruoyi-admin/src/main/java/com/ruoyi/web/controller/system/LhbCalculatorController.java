package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.web.controller.tool.RedisService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LhbCalculator;
import com.ruoyi.system.service.ILhbCalculatorService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
@Controller
@RequestMapping("/lhb/jg")
public class LhbCalculatorController extends BaseController
{
    @Autowired
    private RedisService redisService;
    @Autowired
    private ILhbCalculatorService lhbCalculatorService;

//    @RequiresPermissions("system:calculator:view")
    @GetMapping()
    public String calculator(ModelMap mmap)
    {
        mmap.put("trade_date_list",redisService.get("trade_date_list"));
        return "system/lhb/jg";
    }

    /**
     * 查询【请填写功能名称】列表
     */
//    @RequiresPermissions("system:calculator:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LhbCalculator lhbCalculator)
    {
        startPage();
        List<LhbCalculator> list = lhbCalculatorService.selectLhbCalculatorList(lhbCalculator);
        return getDataTable(list);
    }

}
