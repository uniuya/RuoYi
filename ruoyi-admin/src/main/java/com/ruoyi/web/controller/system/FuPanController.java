package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.LhbCalculator;
import com.ruoyi.system.service.IDailyZtEastmoneyService;
import com.ruoyi.system.service.ILhbCalculatorService;
import com.ruoyi.web.controller.tool.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Set;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-06-03
 */
@Controller
@RequestMapping("/fupan")
public class FuPanController extends BaseController
{
    @Autowired
    private RedisService redisService;
    @Autowired
    private IDailyZtEastmoneyService dailyZtEastmoneyService;

//    @RequiresPermissions("system:calculator:view")
    @GetMapping()
    public String calculator(ModelMap mmap)
    {
        String today = (String)redisService.get("today");
        Integer tradeDateFlag=Integer.parseInt((String)redisService.get("trade_day_flag"));
        if(tradeDateFlag==1 && (Integer.parseInt((DateUtils.dateTimeNow("HH")))<16)){
            today = (String)redisService.get("today-1");
        }
        mmap.put("hotConceptStocks", JSON.parse((String)redisService.hget("daily_hot_concept_stocks_xgb",today)));
        mmap.put("daily_basic_fp", JSON.parse((String)redisService.hget("daily_basic_fp",today)));
//        mmap.put("north_net_buy", JSON.parse((String)redisService.hget("hgt_north_net_buy",(String)redisService.get("today"))));
//        mmap.put("upcount_daily", JSON.parse((String)redisService.hget("upcount_daily",(String)redisService.get("today"))));
        mmap.put("daily_bk", JSON.parse((String)redisService.hget("dailyConceptStocks",today)));
        mmap.put("daily_report", JSON.parse((String)redisService.hget("daily_zt_report",today)));
        mmap.put("lbs",  dailyZtEastmoneyService.selectLbByTradeDate(today));
        mmap.put("today",  today);
        return "system/fupan/index";
    }
    @GetMapping("/pic")
    public String calculatorPic(ModelMap mmap)
    {
        String today = (String)redisService.get("today");
        Integer tradeDateFlag=Integer.parseInt((String)redisService.get("trade_day_flag"));
        if(tradeDateFlag==1 && (Integer.parseInt((DateUtils.dateTimeNow("HH")))<16)){
            today = (String)redisService.get("today-1");
        }
        mmap.put("hotConceptStocks", JSON.parse((String)redisService.hget("daily_hot_concept_stocks_xgb",today)));
        mmap.put("daily_basic_fp", JSON.parse((String)redisService.hget("daily_basic_fp",today)));
//        mmap.put("north_net_buy", JSON.parse((String)redisService.hget("hgt_north_net_buy",(String)redisService.get("today"))));
//        mmap.put("upcount_daily", JSON.parse((String)redisService.hget("upcount_daily",(String)redisService.get("today"))));
        mmap.put("daily_bk", JSON.parse((String)redisService.hget("dailyConceptStocks",today)));
//        Set<Object> daily_report =redisService.sReverseRangeByNum("daily_zt_report_socre",1);
//        mmap.put("daily_report", JSON.parse((String)daily_report.stream().findFirst().get()));
        mmap.put("daily_report", JSON.parse((String)redisService.hget("daily_zt_report",today)));
        mmap.put("lbs",  dailyZtEastmoneyService.selectLbByTradeDate(today));
        mmap.put("today",  today);
        return "system/fupan/index_pic";
    }




}
